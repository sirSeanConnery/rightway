using System.Collections;
using System.Collections.Generic;
using Gameplay.Weapons.Projectiles;
using Gameplay.Weapons;
using UnityEngine;

public class Rocket : Projectile
{

    [SerializeField]
    private float _explosionRadius;
    
    [SerializeField]
    private float _lifeTime;

    [SerializeField]
    private GameObject _explosionPrefab;

    protected override void Move(float speed)
    {
        transform.Translate(speed * Time.deltaTime * Vector3.up);
    }

    private void Start()
    {
        if (!_explosionPrefab)
            Debug.LogError("Rocket has no reference to explosion prefab");

        StartCoroutine("ExplosiveDetonator", _lifeTime);
    }

    protected override void OnCollisionEnter2D(Collision2D other)
    {
        var collision = other.gameObject.GetComponent<IDamagable>();
        // Animate Explode();
        if (collision != null
            && collision.BattleIdentity != BattleIdentity)
        {
            var hitColliders = Physics2D.OverlapCircleAll(transform.position, _explosionRadius);

            foreach (var hitCollider in hitColliders)
            {
                var damagableObject = hitCollider.gameObject.GetComponent<IDamagable>();
                if (damagableObject != null)
                {
                    damagableObject.ApplyDamage(this);
                }
            }

            Explode();
        }
    }

    private void Explode()
    {
        StopCoroutine("ExplosiveDetonator");
        var explosion = Instantiate(_explosionPrefab, transform.position, Quaternion.identity);
        Destroy(gameObject);
        
    }

    IEnumerator ExplosiveDetonator(float lifeTime)
    {
        yield return new WaitForSeconds(lifeTime);
        Explode();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, _explosionRadius);
    }
}
