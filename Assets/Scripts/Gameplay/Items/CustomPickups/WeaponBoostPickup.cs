using System.Collections;
using System.Collections.Generic;
using Gameplay.Spaceships;
using UnityEngine;

public class WeaponBoostPickup : ItemPickup
{
    [SerializeField]
    private float _boostingAmount;

    public override void PickUp(Spaceship ship)
    {
        ship.ApplyEnergyBoost(_boostingAmount);
        Destroy(gameObject);
    }
}

