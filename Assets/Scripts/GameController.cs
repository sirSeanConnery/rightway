using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gameplay.Spaceships;

public class GameController : MonoBehaviour
{
    private static int _score;

    [SerializeField]
    private Spaceship _player;

    [SerializeField]
    private GameObject _gameOverUI;

    public delegate void OnScoreUpdated(int score);
    public static OnScoreUpdated onScoreUpdated;

    private void Awake()
    {
        _score = 0;
        Spaceship.onShipExplode += UpdateScore;


        if (!_player)
            Debug.LogError("Player object is not referenced in GameController");
        _player.onPlayerShipDestroyed += displayGameOver;
    }
    
    void Start()
    {
        if (_gameOverUI)
            _gameOverUI.SetActive(false);        
    }

    private void FixedUpdate()
    {
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }

    private void OnDisable()
    {
        Spaceship.onShipExplode -= UpdateScore;
        _player.onPlayerShipDestroyed -= displayGameOver;
    }
    public static int currentScore()
    {
        return _score;
    }

    public void displayGameOver()
    {
        Time.timeScale = 0;
        _gameOverUI.SetActive(true);
    }

    private void UpdateScore(int bounty)
    {
        _score += bounty;

        onScoreUpdated?.Invoke(_score);
    }
}
