using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gameplay.Spaceships;
using Gameplay.Weapons;

public class ItemPickup : MonoBehaviour, IPickable
{
    public virtual void PickUp(Spaceship ship)
    {
    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        var ship = collision.gameObject.GetComponent<Spaceship>();
        if(ship && ship.BattleIdentity == UnitBattleIdentity.Ally)
        {
            PickUp(ship);
        }
    }
}
