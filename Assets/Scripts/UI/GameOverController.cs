using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Gameplay.UI
{
    public class GameOverController : MonoBehaviour
    {
        public void QuitGame()
        {
            Application.Quit();
        }

        public void PlayAgain()
        {
            Time.timeScale = 1;
            SceneManager.LoadScene("GameScene");
        }

    }
}
