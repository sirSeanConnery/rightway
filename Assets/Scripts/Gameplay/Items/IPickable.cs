using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gameplay.Spaceships;

public interface IPickable 
{
    public void PickUp(Spaceship ship);
}
