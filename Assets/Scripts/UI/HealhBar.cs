using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Gameplay.Spaceships;
using TMPro;

namespace Gameplay.UI
{
    public class HealhBar : MonoBehaviour
    {

        [SerializeField]
        private Slider _slider;

        [SerializeField]
        private Gradient _gradient;

        [SerializeField]
        private Image _fill;

        [SerializeField]
        private Spaceship _owner;

        [SerializeField]
        private TextMeshProUGUI _hullIntegrityText;

        // Start is called before the first frame update
        void Awake()
        {
            if (_owner)
            {
                _owner.onShipMaxHPChanged += SetMaxHullIntegrity;
                _owner.onShipHPChanged += UpdateHealthBar;
            }
        }

        private void OnDisable()
        {
            _owner.onShipMaxHPChanged -= SetMaxHullIntegrity;
            _owner.onShipHPChanged -= UpdateHealthBar;
        }

        void SetMaxHullIntegrity(float maxHullIntegrity)
        {
            _slider.maxValue = (int)maxHullIntegrity;
            _slider.value = (int)maxHullIntegrity;

            _fill.color = _gradient.Evaluate(1f);

            _hullIntegrityText?.SetText(maxHullIntegrity.ToString());
        }

        // Update is called once per frame
        void UpdateHealthBar(float hullIntegrity)
        {
            _slider.value = (int)hullIntegrity;
            _fill.color = _gradient.Evaluate(_slider.normalizedValue);
            _hullIntegrityText?.SetText(hullIntegrity.ToString());
        }
    }
}
