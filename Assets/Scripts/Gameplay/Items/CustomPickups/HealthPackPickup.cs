using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gameplay.Spaceships;

public class HealthPackPickup : ItemPickup 
{
    [SerializeField]
    private float _repairingAmount = 100f;

    public override void PickUp(Spaceship ship)
    {
        ship.RestoreHP(_repairingAmount);
        Destroy(gameObject);
    }
}
