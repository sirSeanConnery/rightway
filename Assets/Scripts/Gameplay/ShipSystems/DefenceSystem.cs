using System.Collections.Generic;
using UnityEngine;
using Gameplay.Weapons;
using System;

namespace Gameplay.ShipSystems
{
    public class DefenceSystem : MonoBehaviour
    {

        [SerializeField]
        private float _maxHullIntegrity;

        [SerializeField]
        private float _currentHullIntegrity;

        public delegate void OnHullIntegrityChanged();
        public OnHullIntegrityChanged onHullIntegrityChanged;

        public delegate void OnHullIntegrityCompromised();
        public OnHullIntegrityCompromised onHullIntegrityCompromised;

        private void Start()
        {
            _currentHullIntegrity = _maxHullIntegrity;
        }

        public float currentHullIntegrity()
        {
            return _currentHullIntegrity;
        }

        public float maxHullIntegrity()
        {
            return _maxHullIntegrity;
        }

        public void ReceiveDamage(float damage)
        {
            _currentHullIntegrity -= damage;
            onHullIntegrityChanged?.Invoke();

            if (_currentHullIntegrity <= 0)
            {
                //Ship about to Explode
                onHullIntegrityCompromised?.Invoke();
            }

        }

        public void RepairHull(float repairingAmount)
        {
            _currentHullIntegrity += repairingAmount;

            if (_currentHullIntegrity > _maxHullIntegrity)
                _currentHullIntegrity = _maxHullIntegrity;

            onHullIntegrityChanged?.Invoke();
        }
    }
}

