using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Gameplay.UI
{

    public class ScoreController : MonoBehaviour
    {
        // Start is called before the first frame update
        [SerializeField]
        private TextMeshProUGUI _scoreText;

        private void Awake()
        {
            GameController.onScoreUpdated += UpdateText;
        }

        void Start()
        {
            UpdateText(GameController.currentScore());
        }

        private void OnDisable()
        {
            GameController.onScoreUpdated -= UpdateText;
        }

        void UpdateText(int newScore)
        {
            _scoreText.text = newScore.ToString();
        }
    }
}
