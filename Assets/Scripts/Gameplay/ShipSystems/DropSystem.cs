using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

namespace Gameplay.ShipSystems
{
    [System.Serializable]
    public class ItemDropTable
    {
        public GameObject _prefab;
        public int _dropRate;
    }

    public class DropSystem : MonoBehaviour
    {
        [SerializeField]
        ItemDropTable[] _itemPrefabs;

        public static Random random = new Random();

        public void RollForDrop()
        {
            int prefabIndex;
            if (_itemPrefabs.Length > 0)
            {
                prefabIndex = random.Next(0, _itemPrefabs.Length);
                var item = _itemPrefabs[prefabIndex];
                var dropChance = random.Next(0, 100);
                if (dropChance <= item._dropRate)
                {
                    Instantiate(item._prefab, transform.position, Quaternion.identity);
                }
            }
        }
    }
}
