﻿using System;
using Gameplay.ShipControllers;
using Gameplay.ShipSystems;
using Gameplay.Weapons;
using UnityEngine;

namespace Gameplay.Spaceships
{
    public class Spaceship : MonoBehaviour, ISpaceship, IDamagable
    {
        [SerializeField]
        private ShipController _shipController;

        [SerializeField]
        private MovementSystem _movementSystem;

        [SerializeField]
        private WeaponSystem _weaponSystem;

        [SerializeField]
        private DefenceSystem _defenceSystem;

        [SerializeField]
        private DropSystem _dropSystem;

        [SerializeField]
        private UnitBattleIdentity _battleIdentity;

        [SerializeField]
        private int _bounty;

        [SerializeField]
        private int _collisionDamage;
        
        [SerializeField]
        private GameObject _explosionPrefab;


        public MovementSystem MovementSystem => _movementSystem;
        public WeaponSystem WeaponSystem => _weaponSystem;

        public UnitBattleIdentity BattleIdentity => _battleIdentity;

        public delegate void OnShipHPChanged(float hullIntegrity);
        public OnShipHPChanged onShipHPChanged;

        public delegate void OnShipMaxHPChanged(float maxHullIntegrity);
        public OnShipMaxHPChanged onShipMaxHPChanged;

        public delegate void OnShipExplode(int bounty);
        public static OnShipExplode onShipExplode;

        public delegate void OnPlayeShipDestroyed();
        public OnPlayeShipDestroyed onPlayerShipDestroyed;

        private void Awake()
        {
            _defenceSystem.onHullIntegrityChanged += HandleHullIntegrityChange;
            _defenceSystem.onHullIntegrityCompromised += Explode;
        }

        private void Start()
        {
            _shipController.Init(this);
            _weaponSystem.Init(_battleIdentity);

            onShipMaxHPChanged?.Invoke(_defenceSystem.maxHullIntegrity());
        }

        private void OnDisable()
        {
            _defenceSystem.onHullIntegrityChanged -= HandleHullIntegrityChange;
            _defenceSystem.onHullIntegrityCompromised -= Explode;
        }

        public void ApplyDamage(IDamageDealer damageDealer)
        {
            _defenceSystem.ReceiveDamage(damageDealer.Damage);
        }

        public void ApplyCollisionDamage(float damage)
        {
            _defenceSystem.ReceiveDamage(damage);
        }

        public void RestoreHP(float repairingAmount)
        {
            _defenceSystem.RepairHull(repairingAmount);
        }

        public void ApplyEnergyBoost(float boostAmount)
        {
            _weaponSystem.BoostWeapons(boostAmount);
        }
               
        private void HandleHullIntegrityChange()
        {
            onShipHPChanged?.Invoke(_defenceSystem.currentHullIntegrity());
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            var damagableObject = other.gameObject.GetComponent<IDamagable>();

            if (damagableObject != null
                && damagableObject.BattleIdentity != Gameplay.Weapons.UnitBattleIdentity.Enemy)
            {
                var spaceship = other.gameObject.GetComponent<Spaceship>();
                if (spaceship)
                    spaceship.ApplyCollisionDamage(_collisionDamage);

                Destroy(gameObject);
            }
        }

        private void Explode()
        {
            //Decide what item will drop from ship wreck
            _dropSystem?.RollForDrop();
            //Play desctruction animation and then remove obj from scene
            var explosion = Instantiate(_explosionPrefab, transform.position, Quaternion.identity);

            //Notifies Game with bounty(score) for killing enemy ships
            if (_battleIdentity != UnitBattleIdentity.Ally)
                onShipExplode?.Invoke(_bounty);
            else
                onPlayerShipDestroyed?.Invoke();

            Destroy(gameObject);
        }
    }
}
