using System.Collections;
using Gameplay.ShipSystems;
using UnityEngine;
using Random = System.Random;

public class WarBirdShipController : EnemyShipController
{
    private float _direction;

    [SerializeField]
    private int _directionChangeTimeOut;

    public static Random random = new Random();

    private void Start()
    {
        _direction = random.Next(-1, 2);
        StartCoroutine("FlipDirection");
    }
    protected override void ProcessHandling(MovementSystem movementSystem)
    {
        movementSystem.LongitudinalMovement(Time.deltaTime);
        movementSystem.LateralMovement(Time.deltaTime * _direction);
    }

    IEnumerator FlipDirection()
    {
        yield return new WaitForSeconds(_directionChangeTimeOut);
        _direction = _direction *  random.Next(-1, 2);
        StartCoroutine("FlipDirection");
    }
}
