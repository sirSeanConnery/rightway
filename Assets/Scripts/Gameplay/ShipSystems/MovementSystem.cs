﻿using UnityEngine;

namespace Gameplay.ShipSystems
{
    public class MovementSystem : MonoBehaviour
    {

        [SerializeField]
        private float _lateralMovementSpeed;

        [SerializeField]
        private float _longitudinalMovementSpeed;

        private Bounds _bounds;

        private void Start()
        {
            var renderer = transform.GetComponentInChildren<SpriteRenderer>();
            if (!renderer)
            {
                Debug.LogWarning("Couldnt find sprite renderer for ship " + transform.name);
            }
            _bounds = renderer.bounds;
        }

        public void LateralMovement(float amount)
        {
            Move(amount * _lateralMovementSpeed, Vector3.right);
        }

        public void LongitudinalMovement(float amount)
        {
            Move(amount * _longitudinalMovementSpeed, Vector3.up);
        }


        private void Move(float amount, Vector3 axis)
        {
            transform.Translate(amount * axis.normalized);
        }
    }
}
